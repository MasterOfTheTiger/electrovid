const colorThief = new ColorThief();

const loadingIconHTML = '<div class="loader"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>';

document.getElementById('search').onkeydown = function (key) {
    if (key.keyCode == 13) {
        searchMovie();
    }
}

// Derived from https://stackoverflow.com/posts/1727846/ by medoix
document.onkeydown = function (key){
    if (key.ctrlKey && key.keyCode==70){
        document.getElementById('search').focus();
    } else if (key.keyCode == 27) {
        // Closes movieData popup when escape key is pressed
        document.getElementById('movieData').style.display = 'none';
    }
}

// document.getElementById('search').onchange = function () {
//     if (document.getElementById('search').innerText == '') {
//         loadHome();
//     }
// }

const openMovie = function (movie) {
    document.getElementById('movieData').style.display = 'block';
    (async () => {
        console.log('Loading movie: ' + movie);
        document.getElementById('movieData').innerHTML = loadingIconHTML;
        
        try {
            const response = await got('https://yts.am/api/v2/movie_details.json?movie_id=' + movie);
            
            let data;
            let movieData;
            try {
                data = JSON.parse(response.body);
                movieData = data.data['movie'];
                console.log(data);
            } catch (error) {
                data = response.body;
            }

            buildMovie(movieData);
        } catch (error) {
            //document.getElementById('movieData').innerText = 'Failed to load movie data';
            document.getElementById('movieData').style.display = 'none';
            throw new Error(error);
            //console.log(error.response.body);
        }
        
    })();
}



// Adds more items to homepage after scrolling to bottom:
window.onscroll = function() {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
        (async () => {
            console.log('Loading YTS API data');
            page++;
            let response;
            try {
                response = await got(requestURL + page);
                
                let data;
                let movies;
                try {
                    data = JSON.parse(response.body);
                    movies = data.data.movies;
                    console.log(movies);
                } catch (error) {
                    data = response.body;
                    console.log(data)
                }
                
                loadMovies(movies, true);
            } catch (error) {
                throw new Error(error);
            }
            
        })();
    }
};



// Builds an element an individual movieData into special popup
const buildMovie = function (movieData) {
    const element = document.createElement('div');

    let magnet = buildMagnet(movieData.torrents[0].hash);
    console.log('Magnet link: ' + magnet);

    let cover = document.createElement('img');
    cover.id = 'movie-cover';
    cover.src = movieData['large_cover_image'];

    let title = document.createElement('h3');
    title.innerText = movieData['title'];

    let rating = movieData['mpa_rating'];
    if (rating == '') {
        rating = 'Rating unavailable';
    }

    let quickInfo = document.createElement('div');
    quickInfo.id = 'movie-info';
    quickInfo.innerText = movieData['year'] + ' ~ ' + rating + ' ~ ' + movieData['runtime'] + ' mins ~ IMDb ' + movieData['rating']; 

    let description = document.createElement('p');
    description.innerText = movieData['description_full'];
    description.id = 'movie-description';
    
    let closeMovie = document.createElement('div');
    closeMovie.innerHTML = '<i class="fa fa-times" aria-hidden="true"></i>';
    closeMovie.id = 'close-movie';

    let start = document.createElement('button');
    start.id = 'start-movie';
    start.innerText = 'Watch';

    element.appendChild(cover);
    element.appendChild(closeMovie);
    element.appendChild(title);
    element.appendChild(quickInfo);
    element.appendChild(description);
    element.appendChild(start)
    
    document.getElementById('movieData').innerHTML = element.innerHTML;

    document.getElementById('movieData').style.backgroundImage = 'url(\'' + movieData['background_image'] + '\')';

    document.getElementById('close-movie').addEventListener('click', function () {
        document.getElementById('movieData').style.display = 'none';
    });
    document.getElementById('start-movie').addEventListener('click', function () {
        startMovie(magnet);
    });

    // Change button color to match theme of movie
    let colorData;
    let img = new Image();
    img.src = movieData['large_cover_image'];
    img.onload = function () {   
        colorData = colorThief.getColor(img);
        console.log('This: ' + colorData + ' is the dominant color of the cover image.');
        document.getElementById('start-movie').style.backgroundColor = 'rgb(' + colorData + ')';
    }
}



// Opens certain areas such as settings, info, and more
const openArea = function (area) {
    const element = document.getElementById('area-' + area);
    element.style.display = 'block';
}