const got = require('got');
const torrentSearch = require('torrent-search-api');
const providers = torrentSearch.getProviders();
//torrentSearch.enableProvider('Torrent9');
//const torrents = await TorrentSearchApi.search('1080', 'Movies', 20);

const WebTorrent = require('webtorrent')
if (WebTorrent.WEBRTC_SUPPORT) {
    console.log('WebRTC is available.');
} else {
    console.log('WebRTC is not available!');
}

// Search for basic homepage movies
let page;
let requestURL;
const loadHome = function (type) {
    document.getElementById('search').value = '';
    (async () => {
        document.getElementById('movies').innerHTML = loadingIconHTML;
        console.log('Loading YTS API data');

        for (let i = 0; i < document.getElementsByClassName('genre').length; i++) {
            const element = document.getElementsByClassName('genre')[i];
            element.classList.remove("selected");
        }
        document.getElementById('genre-' + type).classList.add("selected");
        
        let response;
        try {
            requestURL = 'https://yts.am/api/v2/list_movies.json?limit=30&minimum_rating=0&sort_by=download_count&genre=' + type + '&page=';
            response = await got(requestURL + '1');
            page = 1;
            
            let data;
            let movies;
            try {
                data = JSON.parse(response.body);
                movies = data.data.movies;
                console.log(movies);
            } catch (error) {
                data = response.body;
                console.log(data)
            }
            
            loadMovies(movies);
        } catch (error) {
            document.getElementById('movies').innerText = 'Failed to load movies';
            throw new Error(error);
            //console.log(error.response.body);
        }
        
    })();
}
loadHome('all');


// Plays movie with webtorrent
//let globalTorrent;
let client;
let time;
const watchMovie = function (torrentId) {
    document.getElementById('downloadSpeed').innerText = '';
    document.getElementById('estTime').innerText = '';
    document.getElementById('num-peers').innerText = '';
    go = true;
    //document.getElementById('movie').innerText = 'Video loading...'
    console.log('Loading video...');
    
    client = new WebTorrent()

    client.add(torrentId, function (torrent) {

        window.globalTorrent = torrent;
        // Torrents can contain many files. Let's use the .mp4 file
        var file = torrent.files.find(function (file) {
            return file.name.endsWith('.mp4')
        })

        torrent.on('download', function (bytes) {
            //console.log('total downloaded: ' + torrent.downloaded)
            document.getElementById('downloadSpeed').innerText = Math.round(torrent.downloadSpeed / 1000) + ' KB/s';
            document.getElementById('num-peers').innerText = torrent.numPeers;
            //console.log('progress: ' + torrent.progress)
        })

        console.log('Passed to DOM.');
        console.log(torrent.path);
        
        time = setInterval(() => {
            document.getElementById('estTime').innerText = milliToTime(torrent.timeRemaining);
        }, 500);

        setInterval(() => {
            if (go = false) {
                torrent.destroy(function () {console.log('Removed torrent.')});
            }
        }, 200);

        // Display the file by adding it to the DOM. Supports video, audio, image, etc. files
        document.getElementById('movie').innerHTML = '';
        file.appendTo('#movie')

    })

}

// Modified from https://stackoverflow.com/posts/19700358/ by Dusht
const milliToTime = function (milli) {
    var milliseconds = parseInt((milli % 1000) / 100),
    seconds = Math.floor((milli / 1000) % 60),
    minutes = Math.floor((milli / (1000 * 60)) % 60),
    hours = Math.floor((milli / (1000 * 60 * 60)) % 24);
  
    hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? "0" + seconds : seconds;
  
    return hours + ":" + minutes + ":" + seconds + "." + milliseconds;
  }

// function reactKey(key) {
//     if(key.keyCode==13) {
//         searchMovie();
//     }
//  }

const searchMovie = function () {
    (async () => {
        for (let i = 0; i < document.getElementsByClassName('genre').length; i++) {
            const element = document.getElementsByClassName('genre')[i];
            element.classList.remove("selected");
        }
        document.getElementById('movies').innerHTML = loadingIconHTML;
        let query = document.getElementById('search').value;

        console.log(query);
        let response;
        try {
            requestURL = 'https://yts.am/api/v2/list_movies.json?limit=30&query_term=' + query + '&page=';
            response = await got(requestURL + page);
            
            let data = JSON.parse(response.body);
            let movies = data.data.movies;
            console.log(movies);
            
            loadMovies(movies);
        } catch (error) {
            document.getElementById('movies').innerText = 'Failed to load movies';
            throw new Error(error);
        }
    })();
}


// Trackers variable and buildMagnet function derived from https://github.com/Munter/yify-query/blob/master/lib/index.js Copyright (c) 2014 Peter Müller munter@fumle.dk under the MIT License
let trackers = [
    'udp://open.demonii.com:1337',
    'udp://tracker.istole.it:80',
    'http://tracker.yify-torrents.com/announce',
    'udp://tracker.publicbt.com:80',
    'udp://tracker.openbittorrent.com:80',
    'udp://tracker.coppersurfer.tk:6969',
    'udp://exodus.desync.com:6969',
    'http://exodus.desync.com:6969/announce'
];

const buildMagnet = function (hash) {
    //let template = 'magnet:?xt=urn:btih:{HASH}&dn={TITLE}&tr=' + trackers.join('&tr=');
    let template = 'magnet:?xt=urn:btih:' + hash + '&tr=' + trackers.join('&tr=');
    return template;
};
// End derived code

// Build all movies from YTS into HTML in #movies element
const buildMovies = function (movies) {
    let element = document.createElement('div');
    // element.className = 'movies';

    for (let i = 0; i < movies.length; i++) {
        const movie = movies[i];
        let el = document.createElement('div');
        //el.style.backgroundImage = 'url(\'' + movie['medium_cover_image'] + '\')';
        let img = document.createElement('div');
        img.style.backgroundImage = 'url(\"' + movie['medium_cover_image'] + '\")';
        img.className = 'img';
        let name = document.createElement('span');
        name.innerText = movie['title'];
        el.className = 'movie';
        el.id = 'movie-' + movie['id'];
        
        let hoverItems = document.createElement('div');
        hoverItems.className = 'hover-items';
        let hoverTitle = document.createElement('span');
        hoverTitle.innerText = movie['title_long'];
        hoverItems.appendChild(hoverTitle);
        img.appendChild(hoverItems);

        el.appendChild(img);
        el.appendChild(name);
        element.appendChild(el);
    }
    
    return element;
};

const loadMovies = function (movieData, append = false) {
    const element = buildMovies(movieData);
    
    if (append == false) {
        document.getElementById('movies').innerHTML = '';
    }
    if (typeof(document.getElementsByClassName('movies')[0]) == 'undefined') {
        let moviesEl = document.createElement('div');
        moviesEl.className = 'movies';
        document.getElementById('movies').appendChild(moviesEl);
    }
    
    document.getElementsByClassName('movies')[0].innerHTML = document.getElementsByClassName('movies')[0].innerHTML + element.innerHTML;

    let el = document.getElementById('movies');
    let elements = el.getElementsByClassName('movie');
    for (let i = 0; i < elements.length; i++) {
        const element = elements[i];
        element.addEventListener('click', function () {
            openMovie(this.id.split('movie-')[1]);
        });
    }
}


// open the movie popup and start torrenting the movie
const startMovie = function (magnet) {
    document.getElementById('movieBox').style.display = 'block';
    document.getElementById('movie').innerHTML = loadingIconHTML;
    

    watchMovie(magnet);
}

const killMovie = function () {
    client.destroy();
    document.getElementById('movie').innerHTML = '';
    document.getElementById('movieBox').style.display='none';
    clearInterval(time);
}