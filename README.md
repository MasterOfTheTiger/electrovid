# ElectroVid (name will be changed)

Watch movies with YTS and WebTorrent.

This will be similar to PopcornTime, but with newer features, including the ability to seed to web browsers. 

Current features:
- Search
- Infinite scroll
- Movie pop-up with information about the movie
- Simple information about current download speed and ETA of movie (Release date, description, MPAA rating, runtime) which changes color based on the cover image of the movie

TODO:
- Support other providers than YTS.am in search button with `torrent-search-api`
- Improve video player and provide more information about connection in a cleaner way
- Add settings menu
- Add ability to minimise and re-open the player

## Development

```
git clone https://gitlab.com/MasterOfTheTiger/electrovid.git
cd electrovid
yarn install # or npm install
yarn start # or npm run start
```

Enjoy your dev environment of Electrovid!